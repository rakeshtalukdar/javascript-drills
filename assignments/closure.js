const counterFactory = () => {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

    let counter = 0;
    const counterObj = {
        increment: () => {
            return ++counter;
        },
        decrement: () => {
            return --counter;
        },
    };
    return counterObj;
};

const limitFunctionCallCount = (cb, n) => {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    let cbCounter = null;
    const invokeCb = () => {
        for (let index = 0; index < n; index++) {
            cbCounter = cb(index);
        }
        return cbCounter;
    };
    return invokeCb;
};

//Callback function for limitFunctionCallCount
const callback = (i) => {
    return `I am being called ${i + 1} times`;
};

const cacheFunction = (cb) => {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    const cacheObj = {};
    const invokeCb = (...params) => {
        let paramKeys = '';
        for (let index = 0; index < params.length; index++) {
            paramKeys += params[index] + "_";
        }
        if (paramKeys in cacheObj) {
            return cacheObj[paramKeys];
        } else {
            cacheObj[paramKeys] = cb(params);
            return cacheObj[paramKeys];
        }
    };
    return invokeCb;
};

const getAddition = cacheFunction((args) => {
    let sum = 0;
    for (let index = 0; index < args.length; index++) {
        sum += args[index];
    }
    return sum;
});
