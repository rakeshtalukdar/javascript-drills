const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
}; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

const keys = obj => {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys

    const objKeys = [];
    for (let key in obj) {
        objKeys.push(key);
    }
    return objKeys;
};


const values = obj => {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    const objValues = [];
    for (let key in obj) {
        objValues.push(obj[key]);
    }
    return objValues;
};

const mapObject = (obj, cb) => {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    const result = {};
    for (let key in obj) {
        result[key] = cb(obj[key], key);
    }
    return result;
};

const callbackToMapObject = (value, key) => {
    return typeof value;
};


const pairs = obj => {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    const keyValuePair = [];
    for (let key in obj) {
        keyValuePair.push([key, obj[key]]);
    }
    return keyValuePair;
};

/* STRETCH PROBLEMS */

const invert = obj => {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    const invertObj = {};
    for (let key in obj) {
        invertObj[obj[key]] = key;
    }
    return invertObj;
};


const defaults = (obj, defaultProps) => {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    for (let prop in defaultProps) {
        if (!obj.hasOwnProperty(prop)) {
            obj[prop] = defaultProps[prop];
        }
    }
    return obj;
};
